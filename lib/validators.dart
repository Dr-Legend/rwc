class InputValidators {
  static String nameValidator(String name) =>
      name.length == 0 ? "please enter your name" : null;
  static String emailValidator(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(email))
      return 'Enter Valid Email';
    else
      return null;
  }

  static String phoneValidator(String phone) => _checkStringIsNumber(phone);
  static String messageValidator(String message) =>
      message.length == 0 ? "please enter your message" : null;
  static String _checkStringIsNumber(String phone) {
    int phoneNumber = int.tryParse(phone);
    if (phoneNumber == null) {
      return 'Please enter valid phone number';
    } else if (phoneNumber.toString().length < 2) {
      return 'phone number cannot be less than 2 digits';
    }

    return null;
  }
}
