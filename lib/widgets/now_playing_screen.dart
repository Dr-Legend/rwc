import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:redfieldwesleyanchurch/data/models/audio.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';

class NowPlayingScreen extends StatefulWidget {
  final Records track;
  final RmxAudioPlayer rmxAudioPlayer;
  const NowPlayingScreen(
      {Key key, @required this.rmxAudioPlayer, @required this.track})
      : super(key: key);
  @override
  _NowPlayingScreenState createState() => _NowPlayingScreenState();
}

class _NowPlayingScreenState extends State<NowPlayingScreen> {
  double _seeking;
  double _position = 0;

  int _current = 0;
  int _total = 0;

  String _status = 'none';

  @override
  void initState() {
    super.initState();

    this.widget.rmxAudioPlayer.on('status', (eventName, {dynamic args}) {
      print(eventName + (args ?? "").toString());

      if ((args as OnStatusCallbackData).value != null) {
        if (mounted) {
          setState(() {
            if ((args as OnStatusCallbackData).value['currentPosition'] !=
                null) {
              _current = (args as OnStatusCallbackData)
                  .value['currentPosition']
                  .toInt();
              _total = (((args as OnStatusCallbackData).value['duration']) ?? 0)
                  .toInt();
              _status = (args as OnStatusCallbackData).value['status'];

              if (_current > 0 && _total > 0) {
                _position = _current / _total;
              } else if (!this.widget.rmxAudioPlayer.isLoading &&
                  !this.widget.rmxAudioPlayer.isSeeking) {
                _position = 0;
              }

              if (_seeking != null &&
                  !this.widget.rmxAudioPlayer.isSeeking &&
                  !this.widget.rmxAudioPlayer.isLoading) {
                _seeking = null;
              }
            }
          });
        }
      }
    });
//    if (this.widget.rmxAudioPlayer.currentState != 'playing') {
    _prepare();
//    }
  }

  _prepare() async {
//    List<AudioTrack> playList = [];
//    this.widget.songs.forEach((track) {
//      playList.add(AudioTrack(
//          trackId: '${track.id}',
//          album: this.widget.selectedAlbum,
//          artist: '',
//          albumArt: Constants.WORSHIP_SONGS_THUMBNAILS + track.image,
//          assetUrl: Constants.WORSHIP_SONGS_TRACK + track.song,
//          title: track.name));
//    });
//    await this.widget.rmxAudioPlayer.setPlaylistItems(playList,
//        options: PlaylistItemOptions(
//            retainPosition: false,
//            playFromId: this.widget.selectedSongIndex !=
//                    this.widget.songs[0].id.toString()
//                ? this.widget.selectedSongIndex
//                : this.widget.songs[0].id));
//
//    await _play();
    if (this.widget.rmxAudioPlayer.currentTrack == null) {
      List<AudioTrack> playList = [];

      playList.add(AudioTrack(
          isStream: true,
          trackId: '${this.widget.track.id}',
          album: "rwc",
          artist: '',
          albumArt: Constants.assetPath + this.widget.track.image,
          assetUrl: Constants.assetPath +
              this.widget.track.audioFile.replaceAll(' ', '%20'),
          title: this.widget.track.message));

      await this.widget.rmxAudioPlayer.setPlaylistItems(playList,
          options: PlaylistItemOptions(retainPosition: false));

      await _play();
    } else {
      await _pause();
      this.widget.rmxAudioPlayer.clearAllItems();
      List<AudioTrack> playList = [];
      playList.add(AudioTrack(
          trackId: '${this.widget.track.id}',
          album: "RWC",
          artist: '',
          albumArt: Constants.assetPath + this.widget.track.image,
          assetUrl: Constants.assetPath +
              this.widget.track.audioFile.replaceAll(' ', '%20'),
          title: this.widget.track.message));
      await this.widget.rmxAudioPlayer.setPlaylistItems(playList,
          options: PlaylistItemOptions(retainPosition: false));
      await _play();
    }
  }

  _playFromId(String id) async {
    await this.widget.rmxAudioPlayer.playTrackById(id);
  }

  _play() async {
    await this.widget.rmxAudioPlayer.play().then((_) {
      print(this.widget.rmxAudioPlayer.currentTrack.assetUrl);
      setState(() {});
    }).catchError((e) => print(e));
  }

  _pause() {
    this.widget.rmxAudioPlayer.pause().then((_) {
      setState(() {});
    }).catchError(print);
  }

  @override
  void dispose() {
    this.widget.rmxAudioPlayer.pause();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primaryVariant,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primaryVariant,
        title: Text("Playing " + this.widget.track.message),
      ),
      body: Center(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return _player();
  }

  Widget _player() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  // Box decoration takes a gradient

                  ),
              child: Center(
                child: Container(
                  height: 250,
                  width: 250,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                    boxShadow: this
                                .widget
                                .rmxAudioPlayer
                                .currentTrack
                                ?.albumArt !=
                            null
                        ? [
                            BoxShadow(
                              color: Theme.of(context).colorScheme.secondary,
                              blurRadius: 250,
                            ),
                            BoxShadow(
                              color: Theme.of(context)
                                  .colorScheme
                                  .secondaryVariant,
                              blurRadius: 50,
                            ),
                          ]
                        : [
                            BoxShadow(
                              color: Theme.of(context).colorScheme.onSurface,
                              blurRadius: 250,
                            ),
                          ],
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image:
                            this.widget.rmxAudioPlayer.currentTrack?.albumArt !=
                                    null
                                ? CachedNetworkImageProvider(this
                                    .widget
                                    .rmxAudioPlayer
                                    .currentTrack
                                    .albumArt)
                                : AssetImage('assets/logo/app_Icon.png')),
                  ),
                ),
              ),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                this.widget.rmxAudioPlayer.currentTrack?.title ?? '...',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                    fontSize: 18),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Text(
                _format(_current),
                style: TextStyle(color: Colors.white),
              ),
              new Text(
                _format(_total),
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
          Slider(
            activeColor: Theme.of(context).colorScheme.primary,
            inactiveColor: Theme.of(context).colorScheme.surface,
            value: _seeking ?? _position,
            onChangeEnd: (val) async {
              if (_total > 0) {
                await this.widget.rmxAudioPlayer.seekTo(val * _total);
              }
            },
            onChanged: (val) {
              if (_total > 0) {
                setState(() {
                  _seeking = val;
                });
              }
            },
          ),
          Material(
            color: Theme.of(context).colorScheme.primary,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  onPressed: this.widget.rmxAudioPlayer.skipBack,
                  icon: Icon(
                    Icons.skip_previous,
                    color: Colors.white,
                  ),
                ),
                FloatingActionButton(
                  backgroundColor: Theme.of(context).colorScheme.primaryVariant,
                  heroTag: "Play-pause",
                  onPressed: _onPressed(),
                  child: _icon(),
                ),
                IconButton(
                  onPressed: this.widget.rmxAudioPlayer.skipForward,
                  icon: Icon(
                    Icons.skip_next,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _format(int secs) {
    int sec = secs;

    int min = 0;
    if (secs > 60) {
      min = (sec / 60).floor();
      sec = sec % 60;
    }

    return (min >= 10 ? min.toString() : '0' + min.toString()) +
        ":" +
        (sec >= 10 ? sec.toString() : '0' + sec.toString());
  }

  _onPressed() {
    if (this.widget.rmxAudioPlayer.isLoading ||
        this.widget.rmxAudioPlayer.isSeeking) return null;

    if (this.widget.rmxAudioPlayer.isPlaying) return _pause;

    return _play;
  }

  Widget _icon() {
    if (this.widget.rmxAudioPlayer.isLoading ||
        this.widget.rmxAudioPlayer.isSeeking) {
      return CircularProgressIndicator(
        backgroundColor: Colors.white,
      );
    }

    if (this.widget.rmxAudioPlayer.isPlaying) {
      return const Icon(
        Icons.pause,
        size: 40,
        color: Colors.white,
      );
    }
    if (this.widget.rmxAudioPlayer.isStopped) {
      return const Icon(
        Icons.stop,
        size: 40,
        color: Colors.white,
      );
    }

    return const Icon(
      Icons.play_arrow,
      size: 40,
      color: Colors.white,
    );
  }
}
