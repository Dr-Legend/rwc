import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:redfieldwesleyanchurch/data/models/event_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/widgets/network_image.dart';
import 'package:intl/intl.dart';

class EventDetails extends StatelessWidget {
  final Records article;
  DateFormat dateFormat = DateFormat('dd MMM, yyyy');

  EventDetails({Key key, this.article}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Color(0xFF1995CF)),
        title: Text(
          article.title,
          style: TextStyle(color: Color(0xFF1995CF)),
          overflow: TextOverflow.fade,
          maxLines: 1,
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            children: <Widget>[
              Container(
                  height: 300,
                  width: double.infinity,
                  child: PNetworkImage(
                    Constants.assetPath + article.image,
                    fit: BoxFit.cover,
                  )),
              Container(
                margin: EdgeInsets.fromLTRB(16.0, 250.0, 16.0, 16.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0)),
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      article.title,
                      style: Theme.of(context).textTheme.title,
                    ),
                    SizedBox(height: 10.0),
                    Row(
                      children: [
                        Icon(
                          Icons.date_range,
                          color: Theme.of(context).colorScheme.primaryVariant,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          "${dateFormat.format(article.date)} | ",
                          style: Theme.of(context).textTheme.caption.copyWith(
                              color:
                                  Theme.of(context).colorScheme.primaryVariant),
                        ),
                        Icon(
                          Icons.location_on,
                          color: Theme.of(context).colorScheme.primaryVariant,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          article.location,
                          style: Theme.of(context).textTheme.caption.copyWith(
                              color:
                                  Theme.of(context).colorScheme.primaryVariant),
                        )
                      ],
                    ),
                    SizedBox(height: 10.0),
                    Divider(),
                    SizedBox(
                      height: 10.0,
                    ),
//                    Row(
//                      children: <Widget>[
//                        Icon(Icons.favorite_border),
//                        SizedBox(
//                          width: 5.0,
//                        ),
//                        Text("20.2k"),
//                        SizedBox(
//                          width: 16.0,
//                        ),
//                      ],
//                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Html(data: article.description)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
