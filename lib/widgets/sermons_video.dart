import 'package:auto_orientation/auto_orientation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:intl/intl.dart';
import 'package:redfieldwesleyanchurch/blocs/video/video_bloc.dart';
import 'package:redfieldwesleyanchurch/data/models/video_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SermonsVideo extends StatefulWidget {
  @override
  _SermonsVideoState createState() => _SermonsVideoState();
}

class _SermonsVideoState extends State<SermonsVideo> {
  DateFormat dateFormat = DateFormat('MMM yyyy');
  String monthString;

  @override
  void initState() {
    super.initState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _carouselSize = _screenHeight / 2.1;
    return Scaffold(
      appBar: AppBar(
        iconTheme:
            IconThemeData(color: Theme.of(context).colorScheme.primaryVariant),
        backgroundColor: Theme.of(context).colorScheme.surface,
        title: Text(
          "Videos",
          style: TextStyle(color: Theme.of(context).colorScheme.primaryVariant),
        ),
      ),
      body: BlocBuilder<VideoBloc, VideoState>(
        builder: (BuildContext context, VideoState state) {
          if (state is LoadingVideoState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ErrorVideoState) {
            return Center(
              child: Text(
                  "Unable to fetch Videos please try again later.. ${state.error}"),
            );
          }
          if (state is LoadedVideoState) {
            if (monthString == null) {
              monthString = state.dates.records.first.substrDate;
            }
            return Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SizedBox(
                        height: 50,
                        child: DropdownButton(
                          elevation: 5,
                          icon: Icon(Icons.sort),
                          items: state.dates.records
                              .map((date) => DropdownMenuItem(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(date.substrDate),
                                    ),
                                    value: date.substrDate,
                                  ))
                              .toList(),
                          onChanged: (value) {
                            BlocProvider.of<VideoBloc>(context)
                                .add(LoadVideosEvent(monthString: value));
                            setState(() {
                              monthString = value;
                            });
                          },
                          value: monthString,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: false,
                      itemCount: state.videos.records.length ?? 0,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () async {
                            playYoutubeVideo(
                                state.videos.records[index].youtubeVideoId);
                          },
                          child: VideoCard(
                            videoData: state.videos.records[index],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        },
      ),
    );
  }

  void playYoutubeVideo(String youtubevideoid) {
    FlutterYoutube.playYoutubeVideoByUrl(
      autoPlay: true,
      apiKey: Constants.YOUTUBE_APP_KEY,
      videoUrl: "https://www.youtube.com/watch?v=" + youtubevideoid.trim(),
    );
  }
}

class VideoCard extends StatefulWidget {
  final Records videoData;
  final double radius;
  final double carouselSize;

  VideoCard({
    Key key,
    this.videoData,
    this.radius,
    this.carouselSize,
  }) : super(key: key);

  @override
  _VideoCardState createState() => _VideoCardState();
}

class _VideoCardState extends State<VideoCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 8,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 7,
                    borderRadius: BorderRadius.circular(50),
                    child: SizedBox(
                        height: 70,
                        width: 70,
                        child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: CachedNetworkImage(
                                imageUrl:
                                    'https://img.youtube.com/vi/${this.widget.videoData.youtubeVideoId}/0.jpg',
                                fit: BoxFit.fill,
                              ),
                            ))),
                  ),
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        width: 8,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                        child: Text(
                          this.widget.videoData.title,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(
//                            left: 8.0, bottom: 8.0, right: 8.0),
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                          children: <Widget>[
//                            Text(
//                              DateFormat('dd/MM/yyyy').format(DateTime.parse(
//                                  this.widget.videoData.createdAt)),
//                              style: Theme.of(context).textTheme.caption,
//                            ),
//                            Text(
//                              DateFormat('hh:mm a').format(DateTime.parse(
//                                  this.widget.videoData.createdAt)),
//                              style: Theme.of(context).textTheme.caption,
//                            ),
//                          ],
//                        ),
//                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
