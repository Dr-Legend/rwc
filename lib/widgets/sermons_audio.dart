import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/audio.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/widgets/now_playing_screen.dart';

class SermonsAudio extends StatefulWidget {
  final RmxAudioPlayer rmxAudioPlayer;

  const SermonsAudio({Key key, @required this.rmxAudioPlayer})
      : super(key: key);
  @override
  _SermonsAudioState createState() => _SermonsAudioState();
}

class _SermonsAudioState extends State<SermonsAudio> {
  ApiMethods apiMethods = ApiMethods();
  int _selectedIndex;
  Future<List<Records>> fetchData() async {
    List<Records> records;
    try {
      Audio audios = Audio.fromJson(
          jsonDecode(await apiMethods.requestInGet(Constants.audio)));
      records = audios.records;
    } catch (e) {
      print(e);
    }
    return records;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme:
            IconThemeData(color: Theme.of(context).colorScheme.primaryVariant),
        elevation: 0,
        title: Text(
          "Sermons Audio",
          style: TextStyle(color: Theme.of(context).colorScheme.primaryVariant),
        ),
      ),
      body: FutureBuilder<List<Records>>(
          future: fetchData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting)
              return Center(
                child: CircularProgressIndicator(),
              );
            if (snapshot.hasError) {
              return Center(
                  child: Text(
                "Unable to load data from server",
                style: Theme.of(context).textTheme.caption,
              ));
            }
            if (snapshot.hasData) {
              var songs = snapshot.data;
              return GridView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Hero(
                        tag: songs[index].message,
                        child: Material(
                          elevation: 5,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.bottomLeft,
                                  end: Alignment.topRight,
                                  stops: [
                                    0.5,
                                    0.1,
                                    0.8,
                                    0.8
                                  ],
                                  colors: [
                                    Theme.of(context).colorScheme.primary,
                                    Theme.of(context)
                                        .colorScheme
                                        .primaryVariant,
                                    Theme.of(context).colorScheme.secondary,
                                    Theme.of(context)
                                        .colorScheme
                                        .secondaryVariant,
                                  ]),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
//                              image: DecorationImage(
//                                  fit: BoxFit.fill,
//                                  image: CachedNetworkImageProvider(
//                                      Constants.assetPath +
//                                          "/" +
//                                          songs[index]
//                                              .image
//                                              .replaceAll(" ", "%20"))),
                            ),
                            child: Center(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                ),
                                width: double.maxFinite,
                                height: double.maxFinite,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Material(
                                      color: Colors.transparent,
                                      child: IconButton(
                                          icon: Icon(
                                            FontAwesomeIcons.play,
                                            size: 30,
                                            color:
                                                Colors.white.withOpacity(0.7),
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (_) =>
                                                        NowPlayingScreen(
                                                          rmxAudioPlayer: this
                                                              .widget
                                                              .rmxAudioPlayer,
                                                          track: songs[index],
                                                        )));
                                          }),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 8.0, right: 8.0),
                                      child: Text(
                                        songs[index].message,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ));
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3),
              );
            }
            return Center(child: Text("Nothing to show here"));
          }),
    );
  }
}
