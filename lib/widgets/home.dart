import 'dart:convert';

import 'package:banner_view/banner_view.dart';
import 'package:flutter/material.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/home_images_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/widgets/network_image.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ApiMethods apiMethods = ApiMethods();
  Future<List<Records>> fetchData() async {
    try {
      HomeImages homeImages = HomeImages.fromJson(
          jsonDecode(await apiMethods.requestInGet(Constants.home_images)));
      return homeImages.records;
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Records>>(
        future: fetchData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting)
            return Center(
              child: CircularProgressIndicator(),
            );
          if (snapshot.hasError) {
            return Center(
                child: Text(
              "Unable to load data from server",
              style: Theme.of(context).textTheme.caption,
            ));
          }
          if (snapshot.hasData) {
            return BannerView(
              snapshot.data
                  .map((image) =>
                      PNetworkImage(Constants.assetPath + image.image))
                  .toList(),
              log: false,
              autoRolling: snapshot.data.isNotEmpty,
              indicatorSelected: Container(
                width: 8.0,
                height: 8.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
              intervalDuration: Duration(seconds: 10),
            );
          }
          return Center(child: Text("Nothing to show here"));
        });
  }
}
