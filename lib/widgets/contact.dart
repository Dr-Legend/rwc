import 'dart:async';
import 'dart:io';

import 'package:checkbox_formfield/checkbox_formfield.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:map_launcher/map_launcher.dart' hide MapType;
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/contact_form_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/validators.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:url_launcher/url_launcher.dart';

class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  GlobalKey<FormState> formKey = GlobalKey();
  FocusScopeNode _node = FocusScopeNode();
  ApiMethods apiMethods = ApiMethods();
  String churchContactNumber = "6053021001";
  ContactForm form;
  bool pastorOnly = false;

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  Completer<GoogleMapController> _controller = Completer();
  static final CameraPosition _kGoogleInitial = CameraPosition(
    target: LatLng(44.8641726, -98.522985),
    zoom: 17,
  );
  @override
  void initState() {
    super.initState();
    form = new ContactForm();
  }

  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.surface,
          iconTheme: IconThemeData(
              color: Theme.of(context).colorScheme.primaryVariant),
          title: Text(
            "Contact us",
            style:
                TextStyle(color: Theme.of(context).colorScheme.primaryVariant),
          ),
        ),
        body: KeyboardAvoider(
          autoScroll: true,
          child: Padding(
            padding: const EdgeInsets.only(
                left: 30.0, right: 30, top: 20, bottom: 20),
            child: Form(
              onChanged: () => setState(() => _btnController.reset()),
              key: formKey,
              child: FocusScope(
                node: _node,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        'Contact us',
                        style: Theme.of(context).textTheme.title.copyWith(
                            letterSpacing: 3, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      padding: EdgeInsets.all(12),
                      child: Text(
                        'Please use the contact form below to send us a message call us'
                        '.',
                        style: Theme.of(context)
                            .textTheme
                            .caption
                            .copyWith(fontSize: 17),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    TextFormField(
                      validator: InputValidators.nameValidator,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15)),
                          labelText: "Name*",
                          hintText: "Enter your name",
                          prefixIcon: Icon(
                            Icons.person,
                          )),
                      onSaved: (name) => form.name = name,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      onEditingComplete: () => _node.nextFocus(),
                    ),
//              TextFormField(
//                key: UniqueKey(),
//                validator: InputValidators.nameValidator,
//                decoration: InputDecoration(
//                    border: OutlineInputBorder(
//                        borderRadius: BorderRadius.all(Radius.circular(15))),
//                    hintText: "Name",
//                    prefixIcon: Icon(
//                      Icons.person,
//                      color: Colors.blueAccent,
//                    )),
//                onSaved: (name) => form.name = name,
////                textInputAction: TextInputAction.next,
//                keyboardType: TextInputType.text,
//              ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: InputValidators.emailValidator,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          labelText: "Email*",
                          hintText: "Enter your email",
                          prefixIcon: Icon(
                            Icons.email,
                          )),
                      onSaved: (email) => form.email = email,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                      onEditingComplete: () => _node.nextFocus(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: InputValidators.phoneValidator,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          labelText: "Phone*",
                          hintText: "Enter your phone number",
                          prefixIcon: Icon(
                            Icons.phone,
                          )),
                      onSaved: (phone) => form.phone = phone,
                      textInputAction: TextInputAction.next,
                      keyboardType: Platform.isAndroid
                          ? TextInputType.phone
                          : TextInputType.text,
                      onEditingComplete: () => _node.nextFocus(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: InputValidators.messageValidator,
                      maxLines: 8,
                      minLines: 1,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          labelText: "Message*",
                          hintText: "Enter your message",
                          prefixIcon: Icon(
                            Icons.message,
                          )),
                      onSaved: (message) => form.message = message,
                      textInputAction: TextInputAction.newline,
                      keyboardType: TextInputType.multiline,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Checkbox(
                            value: pastorOnly,
                            onChanged: (value) =>
                                setState(() => pastorOnly = value)),
                        Text(
                          "Confidential: for pastor only",
                          style: Theme.of(context).textTheme.caption,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RoundedLoadingButton(
                      height: 35,
                      controller: _btnController,
                      onPressed: () => _submitForm(),
                      animateOnTap: true,
                      color: Theme.of(context).colorScheme.primaryVariant,
                      child: Text(
                        "SUBMIT",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListTile(
                      onTap: () => _launchURL("tel:$churchContactNumber"),
                      leading: Icon(
                        Icons.phone,
                        color: Colors.green,
                      ),
                      title: Text(
                        "Church Contact Number",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      subtitle: Text(
                        "(605)302-1001",
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 300,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: GoogleMap(
                          mapToolbarEnabled: true,
                          markers: {
                            Marker(
                              markerId: MarkerId("initial"),
                              position: LatLng(44.8641726, -98.522985),
                              zIndex: 17,
                              draggable: false,
                            )
                          },
                          initialCameraPosition: _kGoogleInitial,
                          mapType: MapType.normal,
                          zoomControlsEnabled: false,
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                          onTap: (_) {
                            openMapsSheet(context);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  openMapsSheet(context) async {
    try {
      final title = "Redfield Wesleyan Church";
      final description = "Wesleyan church 38519 174th St";
      final coords = Coords(44.8641726, -98.522985);
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                        onTap: () => map.showMarker(
                          coords: coords,
                          title: title,
                          description: description,
                        ),
                        title: Text(map.mapName),
                        leading: Image(
                          image: map.icon,
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    } catch (e) {
      print(e);
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _submitForm() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      pastorOnly ? form.confidential = "YES" : form.confidential = "No";
      try {
        await apiMethods.requestInPost(
            Constants.contact_us, null, form.toJson());
        setState(() {
          _btnController.success();
        });
        await apiMethods.requestInGet(Constants.mail +
            "name=${form.name}&phone=${form.phone}&email=${form.email}&message=${form.message}&confidential=${form.confidential}&subject=We have new contact request");
        Future.delayed(Duration(seconds: 5)).whenComplete(() {
          formKey.currentState.reset();
          setState(() {
            pastorOnly = false;
          });
        });
      } catch (error) {
        print("$error");
        setState(() {
          _btnController.error();
        });
      }
    } else {
      print("error");
      setState(() {
        _btnController.error();
      });
    }
  }
}
