import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/event_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/widgets/article.dart';
import 'package:redfieldwesleyanchurch/widgets/event_details.dart';
import 'package:redfieldwesleyanchurch/widgets/network_image.dart';

class UpcomingEvents extends StatelessWidget {
  final Color primaryColor = Color(0xff6DBE45);
  final Color bgColor = Color(0xFF1995CF);
  final Color secondaryColor = Color(0xFF324558);

  ApiMethods apiMethods = ApiMethods();
  Future<List<Records>> fetchData() async {
    try {
      Events events = Events.fromJson(
          jsonDecode(await apiMethods.requestInGet(Constants.upcoming_events)));
      return events.records;
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: primaryColor,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          textTheme: TextTheme(
            title: TextStyle(
              color: secondaryColor,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(color: secondaryColor),
          actionsIconTheme: IconThemeData(
            color: secondaryColor,
          ),
        ),
      ),
      child: Scaffold(
        backgroundColor: Theme.of(context).buttonColor,
        body: FutureBuilder<List<Records>>(
            future: fetchData(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting)
                return Center(
                  child: CircularProgressIndicator(),
                );
              if (snapshot.hasError) {
                return Center(
                    child: Text(
                  "Unable to load data from server",
                  style: Theme.of(context).textTheme.caption,
                ));
              }
              if (snapshot.hasData) {
                return ListView.separated(
                  padding: const EdgeInsets.all(16.0),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return _buildArticleItem(index, snapshot.data, context);
                  },
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16.0),
                );
              }
              return Center(child: Text("Nothing to show here"));
            }),
      ),
    );
  }

  Widget _buildArticleItem(
      int index, List<Records> snapshot, BuildContext context) {
    debugPrint(Constants.assetPath + snapshot[index].image);
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => EventDetails(
                    article: snapshot[index],
                  ))),
      child: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            Container(
              width: 90,
              height: 90,
              color: bgColor,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [
                      0.8,
                      0.9,
                      1
                    ],
                    colors: [
                      Theme.of(context).colorScheme.surface,
                      Theme.of(context).colorScheme.secondary,
                      Theme.of(context).colorScheme.secondaryVariant,
                    ]),
              ),
              margin: const EdgeInsets.all(16.0),
              child: Row(
                children: <Widget>[
                  Container(
                    height: 100,
//                    color: Colors.blue,
                    width: 80.0,
                    child: PNetworkImage(
                      Constants.assetPath + snapshot[index].image,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const SizedBox(width: 20.0),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          snapshot[index].title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.justify,
                          style: GoogleFonts.comicNeue(
                            color: secondaryColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                        ),
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                  text: "-",
                                  style: Theme.of(context).textTheme.caption),
                              WidgetSpan(
                                child: const SizedBox(width: 5.0),
                              ),
                              TextSpan(
                                  text: "redfieldwesleyan\n",
                                  style: Theme.of(context).textTheme.caption),
                              WidgetSpan(
                                  child: Icon(
                                Icons.location_on,
                                size: 18,
                              )),
                              TextSpan(
                                  text: "${snapshot[index].location}\n",
                                  style: Theme.of(context).textTheme.caption),
                            ],
                          ),
                          style: TextStyle(height: 2.0),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
