import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/about_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  ApiMethods apiMethods = ApiMethods();
  Future<Records> fetchData() async {
    try {
      About about = About.fromJson(
          jsonDecode(await apiMethods.requestInGet(Constants.about_us)));
      return about.records[0];
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme:
            IconThemeData(color: Theme.of(context).colorScheme.primaryVariant),
        backgroundColor: Colors.white,
        elevation: 2,
        title: Text(
          "About Us",
          style: TextStyle(color: Theme.of(context).colorScheme.primaryVariant),
        ),
      ),
      body: FutureBuilder<Records>(
          future: fetchData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting)
              return Center(
                child: CircularProgressIndicator(),
              );
            if (snapshot.hasError) {
              return Center(
                  child: Text(
                "Unable to load data from server",
                style: Theme.of(context).textTheme.caption,
              ));
            }
            if (snapshot.hasData) {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 25.0, vertical: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Material(
                        elevation: 5,
                        color: Color(0xFF3872b1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Service Times:",
                                style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Html(
                                data: snapshot.data.serviceTimes ??
                                    "Sunday School: 9:30am",
                                customEdgeInsets: (node) =>
                                    EdgeInsets.only(bottom: 0),
                                defaultTextStyle: TextStyle(
                                    fontSize: 17, color: Colors.white),
                                showImages: false,
//                              renderNewlines: true,
//                              useRichText: true,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Material(
                        elevation: 5,
                        color: Color(0xFF3872b1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Weekdays activities:",
                                style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Html(
                                data: snapshot.data.weekdayActivities ??
                                    "Sunday School: 9:30am",
                                customEdgeInsets: (node) =>
                                    EdgeInsets.only(bottom: 0),
                                defaultTextStyle: TextStyle(
                                    fontSize: 17, color: Colors.white),
                                showImages: false,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Material(
                        elevation: 5,
                        color: Color(0xFF3872b1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Mondays:",
                                style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Html(
                                data: snapshot.data.mondays ??
                                    "Narcotics Anonymous open group @ 7:00pm",
                                customEdgeInsets: (node) =>
                                    EdgeInsets.only(bottom: 0),
                                defaultTextStyle: TextStyle(
                                    fontSize: 17, color: Colors.white),
                                showImages: false,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
            return Center(child: Text("Nothing to show here"));
          }),
    );
  }
}
