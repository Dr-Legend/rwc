import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:redfieldwesleyanchurch/blocs/video/video_bloc.dart';
import 'package:redfieldwesleyanchurch/widgets/audio_test.dart';
import 'package:redfieldwesleyanchurch/widgets/sermons_audio.dart';
import 'package:redfieldwesleyanchurch/widgets/sermons_video.dart';

class Sermons extends StatelessWidget {
  final RmxAudioPlayer rmxAudioPlayer;

  const Sermons({Key key, this.rmxAudioPlayer}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      padding: EdgeInsets.all(12),
      mainAxisSpacing: 10,
      crossAxisSpacing: 10,
      children: [
        Column(
          children: [
            InkWell(
              borderRadius: BorderRadius.circular(30),
              splashColor: Theme.of(context).accentColor,
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => SermonsAudio(
                            rmxAudioPlayer: rmxAudioPlayer,
                          ))),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Material(
                  elevation: 5,
                  borderRadius: BorderRadius.circular(30),
                  child: Container(
                    child: Center(
                      child: Icon(
                        FontAwesomeIcons.fileAudio,
                        color: Theme.of(context).colorScheme.surface,
                        size: 40,
                      ),
                    ),
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            stops: [
                              0.5,
                              0.1,
                              0.8,
                              0.8
                            ],
                            colors: [
                              Theme.of(context).colorScheme.primary,
                              Theme.of(context).colorScheme.primaryVariant,
                              Theme.of(context).colorScheme.secondary,
                              Theme.of(context).colorScheme.secondaryVariant,
                            ]),
                        borderRadius: BorderRadius.circular(30),
                        color: Theme.of(context).colorScheme.surface),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text("Audio"),
            ),
            SizedBox(
              height: 10,
              child: Container(),
            )
          ],
        ),
        Column(
          children: [
            InkWell(
              borderRadius: BorderRadius.circular(30),
              splashColor: Theme.of(context).accentColor,
              onTap: () {
                BlocProvider.of<VideoBloc>(context).add(LoadVideosEvent());
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => SermonsVideo()));
              },
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Material(
                  elevation: 5,
                  borderRadius: BorderRadius.circular(30),
                  child: Container(
                    child: Center(
                      child: Icon(
                        FontAwesomeIcons.video,
                        color: Theme.of(context).colorScheme.surface,
                        size: 40,
                      ),
                    ),
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            stops: [
                              0.5,
                              0.1,
                              0.8,
                              0.8
                            ],
                            colors: [
                              Theme.of(context).colorScheme.primary,
                              Theme.of(context).colorScheme.primaryVariant,
                              Theme.of(context).colorScheme.secondary,
                              Theme.of(context).colorScheme.secondaryVariant,
                            ]),
                        borderRadius: BorderRadius.circular(30),
                        color: Theme.of(context).colorScheme.surface),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text("Video"),
            ),
            SizedBox(
              height: 10,
              child: Container(),
            )
          ],
        ),
      ],
    );
  }
}
