import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:redfieldwesleyanchurch/blocs/messages/messages_bloc.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/widgets/network_image.dart';

class Messages extends StatelessWidget {
  DateFormat dateFormat = DateFormat('dd/MM/yyyy hh:mm aa');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<MessagesBloc, MessagesState>(
            builder: (BuildContext context, MessagesState state) {
          if (state is LoadingMessagesState) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is LoadedMessagesState &&
              state.messageData.records != null) {
            return Column(
              children: <Widget>[
                Flexible(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      if (state.messageData.records.isNotEmpty) {
                        return _buildFeaturedItem(context,
                            image: Constants.assetPath +
                                state.messageData.records[index].image,
                            title: state.messageData.records[index].title ?? "",
                            subtitle: dateFormat.format(DateTime.parse(
                                state.messageData.records[index].createdAt)));
                      } else {
                        return Container(
                          child: Center(
                            child: Text('No Messages Found!'),
                          ),
                        );
                      }
                    },
                    itemCount: state.messageData.records.length,
                  ),
                ),
              ],
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        }),
      ),
    );
  }

  Widget _buildItem({String title}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Material(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 5.0,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Text(title,
              style: TextStyle(
                fontSize: 20.0,
              )),
        ),
      ),
    );
  }

  Container _buildFeaturedItem(BuildContext context,
      {String image, String title, String subtitle}) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, top: 8.0, right: 16.0, bottom: 16.0),
      child: Material(
        elevation: 5.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: PNetworkImage(
                  image,
                  fit: BoxFit.cover,
                )),
//            Positioned(
//              right: 10.0,
//              top: 10.0,
//              child: FloatingActionButton(
//                heroTag: subtitle,
//                mini: true,
//                backgroundColor: Colors.white,
//                elevation: 5,
//                onPressed: () async {
//                  //Share.share("$title \n ${Constants.RateUs}");
////                  Scaffold.of(context).showSnackBar(SnackBar(
////                      content: Row(
////                    children: <Widget>[
////                      CircularProgressIndicator(),
////                      SizedBox(
////                        width: 12,
////                      ),
////                      Text('Preparing image to send...')
////                    ],
////                  )));
////                  var request = await HttpClient().getUrl(Uri.parse(image));
////                  var response = await request.close();
////                  Uint8List bytes =
////                      await consolidateHttpClientResponseBytes(response);
////                  Scaffold.of(context).hideCurrentSnackBar();
////                  await Share.file(
////                      'Todays Message', 'message.jpg', bytes, 'image/jpg',
////                      text:
////                          'Download Evans Francis App now! \nPlayStore: ${Constants.RateUs} \n Appstore: ${Constants.RateUsIOS}');
//                },
//                child: Icon(
//                  Icons.share,
//                ),
//              ),
//            ),
            title.isNotEmpty
                ? Positioned(
                    bottom: 20.0,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                      color: Colors.black.withOpacity(0.7),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(title,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold)),
//                          Text(subtitle, style: TextStyle(color: Colors.white))
                        ],
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
