import 'dart:io';

import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/prayer_request_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/validators.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class PrayerRequest extends StatefulWidget {
  @override
  _PrayerRequestState createState() => _PrayerRequestState();
}

class _PrayerRequestState extends State<PrayerRequest> {
  GlobalKey<FormState> formKey = GlobalKey();

  ApiMethods apiMethods = ApiMethods();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  Records form;

  final FocusScopeNode _node = FocusScopeNode();

  _submitForm() async {
    _node.unfocus();
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      try {
        await apiMethods.requestInPost(
            Constants.prayer_request, null, form.toJson());
        setState(() {
          _btnController.success();
        });
        await apiMethods.requestInGet(Constants.mail +
            "name=${form.name}&phone=${form.phone}&email=${form.email}&message=${form.message}&subject=We have new prayer request");
        Future.delayed(Duration(seconds: 5))
            .whenComplete(() => formKey.currentState.reset());
      } catch (error) {
        print("$error");
        setState(() {
          _btnController.error();
        });
      }
    } else {
      print("error");
      setState(() {
        _btnController.error();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    form = new Records();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      gestures: [GestureType.onTap, GestureType.onPanUpdateDownDirection],
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.surface,
          iconTheme: IconThemeData(
              color: Theme.of(context).colorScheme.primaryVariant),
          title: Text(
            "Prayer Request",
            style:
                TextStyle(color: Theme.of(context).colorScheme.primaryVariant),
          ),
        ),
        body: KeyboardAvoider(
          autoScroll: true,
          child: Padding(
            padding: const EdgeInsets.only(
                left: 30.0, right: 30, top: 20, bottom: 20),
            child: Form(
              onChanged: () => setState(() => _btnController.reset()),
              key: formKey,
              child: FocusScope(
                node: _node,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        'Prayer Request',
                        style: Theme.of(context).textTheme.title.copyWith(
                            letterSpacing: 3, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      padding: EdgeInsets.all(12),
                      child: Text(
                        'Kindly submit your prayer request for us to pray & intercede for you.',
                        style: Theme.of(context)
                            .textTheme
                            .caption
                            .copyWith(fontSize: 17),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    TextFormField(
                      validator: InputValidators.nameValidator,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).colorScheme.primary),
                              borderRadius: BorderRadius.circular(15)),
                          labelText: "Name*",
                          hintText: "Enter your name",
                          prefixIcon: Icon(
                            Icons.person,
                          )),
                      onSaved: (name) => form.name = name,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      onEditingComplete: _node.nextFocus,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: InputValidators.emailValidator,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          labelText: "Email*",
                          hintText: "Enter your email",
                          prefixIcon: Icon(
                            Icons.email,
                          )),
                      onSaved: (email) => form.email = email,
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                      onEditingComplete: _node.nextFocus,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: InputValidators.phoneValidator,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          labelText: "Phone*",
                          hintText: "Enter your phone number",
                          prefixIcon: Icon(
                            Icons.phone,
                          )),
                      onSaved: (phone) => form.phone = phone,
                      textInputAction: TextInputAction.next,
                      keyboardType: Platform.isAndroid
                          ? TextInputType.phone
                          : TextInputType.text,
                      onEditingComplete: _node.nextFocus,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      validator: InputValidators.messageValidator,
                      maxLines: 8,
                      minLines: 1,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          labelText: "Message*",
                          hintText: "Enter your prayer request",
                          prefixIcon: Icon(
                            Icons.message,
                          )),
                      onSaved: (message) => form.message = message,
                      textInputAction: TextInputAction.newline,
                      keyboardType: TextInputType.multiline,
                      onEditingComplete: () => _submitForm(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    RoundedLoadingButton(
                      height: 35,
                      controller: _btnController,
                      onPressed: () => _submitForm(),
                      animateOnTap: true,
                      color: Theme.of(context).colorScheme.primaryVariant,
                      child: Text(
                        "SUBMIT",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
