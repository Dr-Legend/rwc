import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/video_dates.dart';
import 'package:redfieldwesleyanchurch/data/models/video_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:intl/intl.dart';
part 'video_event.dart';

part 'video_state.dart';

class VideoBloc extends Bloc<VideoEvent, VideoState> {
  ApiMethods apiMethods = ApiMethods();

  @override
  VideoState get initialState => InitialVideoState();

  @override
  Stream<VideoState> mapEventToState(VideoEvent event) async* {
    if (event is LoadVideosEvent) {
      try {
        var response;
        yield LoadingVideoState();
        var dateRespose =
            await apiMethods.requestInGet(Constants.VIDEO_DATES_API);
        ArchiveDates dates = ArchiveDates.fromJson(jsonDecode(dateRespose));
        if (event.monthString == null)
          response = await apiMethods
              .requestInGet(Constants.video + dates.records.first.substrDate);
        if (event.monthString != null)
          response = await apiMethods
              .requestInGet(Constants.video + event.monthString);
        Video videos = Video.fromJson(jsonDecode(response));

        yield LoadedVideoState(videos: videos, dates: dates);
      } on DioError catch (e) {
        yield ErrorVideoState(e);
        print(e);
      } catch (e) {
        yield ErrorVideoState(e);
        print(e);
      }
    }
  }
}
