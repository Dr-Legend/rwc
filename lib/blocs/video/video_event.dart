part of 'video_bloc.dart';

@immutable
abstract class VideoEvent {}

class LoadVideosEvent extends VideoEvent {
  final String monthString;

  LoadVideosEvent({this.monthString});
}
