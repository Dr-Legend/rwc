part of 'messages_bloc.dart';

@immutable
abstract class MessagesEvent {}

@immutable
class LoadMessagesEvent extends MessagesEvent {}
