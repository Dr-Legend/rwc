part of 'messages_bloc.dart';

@immutable
abstract class MessagesState {}

class InitialMessagesState extends MessagesState {}

class LoadingMessagesState extends MessagesState {}

class LoadedMessagesState extends MessagesState {
  final Message messageData;
//  final List<ArchiveDates> dates;

  LoadedMessagesState({this.messageData});
}

class ErrorMessagesState extends MessagesState {
  final dynamic error;

  ErrorMessagesState(this.error);
}
