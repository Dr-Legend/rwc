import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/data/models/messages_model.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';

part 'messages_event.dart';

part 'messages_state.dart';

class MessagesBloc extends Bloc<MessagesEvent, MessagesState> {
  ApiMethods api = ApiMethods();

  @override
  MessagesState get initialState => InitialMessagesState();

  @override
  Stream<MessagesState> mapEventToState(MessagesEvent event) async* {
    if (event is LoadMessagesEvent) {
      yield LoadingMessagesState();
      try {
        var response = await api.requestInGet(Constants.MESSAGE_API);
//        var dateRespose = await api.requestInGet(Constants.BLOGS_DATES_API);
        Message messages = Message.fromJson(jsonDecode(response));
//        List<ArchiveDates> dates = archiveDatesFromJson(dateRespose[0]);
        yield LoadedMessagesState(messageData: messages);
      } catch (e) {
        yield ErrorMessagesState(e);
      }
    }
  }
}
