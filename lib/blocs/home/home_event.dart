part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

@immutable
class ChangeChildEvent extends HomeEvent {
  final int index;

  ChangeChildEvent(this.index);
}
