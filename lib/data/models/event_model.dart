class Events {
  List<Records> _records;

  Events({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  Events.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  String _title;
  String _image;
  String _description;
  DateTime _date;
  String _createdAt;
  String _updatedAt;
  String _location;
  String _time;

  Records(
      {int id,
      String title,
      String image,
      String description,
      DateTime date,
      String createdAt,
      String updatedAt,
      String location,
      String time}) {
    this._id = id;
    this._title = title;
    this._image = image;
    this._description = description;
    this._date = date;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._location = location;
    this._time = time;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get image => _image;
  set image(String image) => _image = image;
  String get description => _description;
  set description(String description) => _description = description;
  DateTime get date => _date;
  set date(DateTime date) => _date = date;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get location => _location;
  set location(String location) => _location = location;
  String get time => _time;
  set time(String time) => _time = time;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _image = json['image'];
    _description = json['description'];
    _date = DateTime.parse(json['date']);
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _location = json['location'];
    _time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['image'] = this._image;
    data['description'] = this._description;
    data['date'] = this._date;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['location'] = this._location;
    data['time'] = this._time;
    return data;
  }
}
