import 'dart:collection';

import 'package:queries/collections.dart';

class ArchiveDates {
  List<Records> _records;

  ArchiveDates({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  ArchiveDates.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      final seen = Set<String>();
      var lists = List<Records>();
      _records = List<Records>();

      json['records'].toSet().toList().forEach((v) {
        lists.add(new Records.fromJson(v));
      });
      //filter unique values
      final unique = lists.where((str) => seen.add(str._substrDate)).toList();

      _records = unique;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  String _substrDate;

  Records({String substrDate}) {
    this._substrDate = substrDate;
  }

  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;

  Records.fromJson(Map<String, dynamic> json) {
    _substrDate = json['substr_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['substr_date'] = this._substrDate;
    return data;
  }
}
