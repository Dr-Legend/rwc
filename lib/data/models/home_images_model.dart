class HomeImages {
  List<Records> _records;

  HomeImages({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  HomeImages.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  int _order;
  String _image;
  String _createdAt;
  String _updatedAt;

  Records(
      {int id, int order, String image, String createdAt, String updatedAt}) {
    this._id = id;
    this._order = order;
    this._image = image;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  int get order => _order;
  set order(int order) => _order = order;
  String get image => _image;
  set image(String image) => _image = image;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _order = json['order'];
    _image = json['image'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['order'] = this._order;
    data['image'] = this._image;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
