class Audio {
  List<Records> _records;

  Audio({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  Audio.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  String _message;
  String _audioFile;
  String _speaker;
  String _date;
  String _series;
  String _createdAt;
  String _updatedAt;
  String _image;

  Records(
      {int id,
      String message,
      String audioFile,
      String speaker,
      String date,
      String series,
      String createdAt,
      String updatedAt,
      String image}) {
    this._id = id;
    this._message = message;
    this._audioFile = audioFile;
    this._speaker = speaker;
    this._date = date;
    this._series = series;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._image = image;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get message => _message;
  set message(String message) => _message = message;
  String get audioFile => _audioFile;
  set audioFile(String audioFile) => _audioFile = audioFile;
  String get speaker => _speaker;
  set speaker(String speaker) => _speaker = speaker;
  String get date => _date;
  set date(String date) => _date = date;
  String get series => _series;
  set series(String series) => _series = series;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get image => _image;
  set image(String image) => _image = image;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _message = json['message'];
    _audioFile = json['audio_file'];
    _speaker = json['speaker'];
    _date = json['date'];
    _series = json['series'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['message'] = this._message;
    data['audio_file'] = this._audioFile;
    data['speaker'] = this._speaker;
    data['date'] = this._date;
    data['series'] = this._series;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['image'] = this._image;
    return data;
  }
}
