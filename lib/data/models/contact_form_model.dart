class ContactForm {
  int _id;
  String _name;
  String _email;
  String _phone;
  String _message;
  String _confidential;
  String _createdAt;
  String _updatedAt;

  ContactForm(
      {int id,
      String name,
      String email,
      String phone,
      String message,
      String confidential,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._name = name;
    this._email = email;
    this._phone = phone;
    this._message = message;
    this._confidential = confidential;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get message => _message;
  set message(String message) => _message = message;
  String get confidential => _confidential;
  set confidential(String confidential) => _confidential = confidential;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  ContactForm.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _email = json['email'];
    _phone = json['phone'];
    _message = json['message'];
    _confidential = json['confidential'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['email'] = this._email;
    data['phone'] = this._phone;
    data['message'] = this._message;
    data['confidential'] = this._confidential;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
