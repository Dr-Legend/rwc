class Blogs {
  List<Records> _records;

  Blogs({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  Blogs.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  String _title;
  String _data;
  DateTime _date;
  String _image;
  Null _likes;
  int _readTime;
  String _createdAt;
  String _updatedAt;

  Records(
      {int id,
      String title,
      String data,
      DateTime date,
      String image,
      Null likes,
      int readTime,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._title = title;
    this._data = data;
    this._date = date;
    this._image = image;
    this._likes = likes;
    this._readTime = readTime;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get data => _data;
  set data(String data) => _data = data;
  DateTime get date => _date;
  set date(DateTime date) => _date = date;
  String get image => _image;
  set image(String image) => _image = image;
  Null get likes => _likes;
  set likes(Null likes) => _likes = likes;
  int get readTime => _readTime;
  set readTime(int readTime) => _readTime = readTime;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _data = json['data'];
    _date = DateTime.parse(json['date']);
    _image = json['image'];
    _likes = json['likes'];
    _readTime = json['read_time'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['data'] = this._data;
    data['date'] = this._date;
    data['image'] = this._image;
    data['likes'] = this._likes;
    data['read_time'] = this._readTime;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
