class Video {
  List<Records> _records;

  Video({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  Video.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  String _title;
  String _youtubeVideoId;
  String _date;
  String _createdAt;
  String _updatedAt;
  String _substrDate;

  Records(
      {int id,
      String title,
      String youtubeVideoId,
      String date,
      String createdAt,
      String updatedAt,
      String substrDate}) {
    this._id = id;
    this._title = title;
    this._youtubeVideoId = youtubeVideoId;
    this._date = date;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._substrDate = substrDate;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get youtubeVideoId => _youtubeVideoId;
  set youtubeVideoId(String youtubeVideoId) => _youtubeVideoId = youtubeVideoId;
  String get date => _date;
  set date(String date) => _date = date;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _youtubeVideoId = json['youtube_video_id'];
    _date = json['date'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _substrDate = json['substr_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['youtube_video_id'] = this._youtubeVideoId;
    data['date'] = this._date;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['substr_date'] = this._substrDate;
    return data;
  }
}
