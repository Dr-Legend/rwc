class Prayer {
  List<Records> _records;

  Prayer({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  Prayer.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  String _name;
  String _email;
  String _phone;
  String _message;
  String _createdAt;
  String _updatedAt;

  Records(
      {int id,
      String name,
      String email,
      String phone,
      String message,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._name = name;
    this._email = email;
    this._phone = phone;
    this._message = message;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get email => _email;
  set email(String email) => _email = email;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get message => _message;
  set message(String message) => _message = message;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _email = json['email'];
    _phone = json['phone'];
    _message = json['message'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['email'] = this._email;
    data['phone'] = this._phone;
    data['message'] = this._message;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
