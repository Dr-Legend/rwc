class About {
  List<Records> _records;

  About({List<Records> records}) {
    this._records = records;
  }

  List<Records> get records => _records;
  set records(List<Records> records) => _records = records;

  About.fromJson(Map<String, dynamic> json) {
    if (json['records'] != null) {
      _records = new List<Records>();
      json['records'].forEach((v) {
        _records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._records != null) {
      data['records'] = this._records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  int _id;
  String _serviceTimes;
  String _weekdayActivities;
  String _mondays;
  String _createdAt;
  String _updatedAt;

  Records(
      {int id,
      String serviceTimes,
      String weekdayActivities,
      String mondays,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._serviceTimes = serviceTimes;
    this._weekdayActivities = weekdayActivities;
    this._mondays = mondays;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get serviceTimes => _serviceTimes;
  set serviceTimes(String serviceTimes) => _serviceTimes = serviceTimes;
  String get weekdayActivities => _weekdayActivities;
  set weekdayActivities(String weekdayActivities) =>
      _weekdayActivities = weekdayActivities;
  String get mondays => _mondays;
  set mondays(String mondays) => _mondays = mondays;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  Records.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _serviceTimes = json['service_times'];
    _weekdayActivities = json['weekday_activities'];
    _mondays = json['mondays'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['service_times'] = this._serviceTimes;
    data['weekday_activities'] = this._weekdayActivities;
    data['mondays'] = this._mondays;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
