import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:flutter/cupertino.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/blocs/home/home_bloc.dart';
import 'package:redfieldwesleyanchurch/blocs/messages/messages_bloc.dart';
import 'package:redfieldwesleyanchurch/blocs/video/video_bloc.dart';
import 'package:redfieldwesleyanchurch/shared/constants.dart';
import 'package:redfieldwesleyanchurch/widgets/blog.dart';
import 'package:redfieldwesleyanchurch/widgets/messages.dart';
import 'package:redfieldwesleyanchurch/widgets/sermons_video.dart';
import 'package:redfieldwesleyanchurch/widgets/upcoming_events.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

//Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
//  print("onLaunch: $message");
////  _showBigPictureNotification(message);
//  // Or do other work.
//}

class NotificationHandler {
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  FirebaseMessaging _fcm = FirebaseMessaging();
  ApiMethods apiMethods = ApiMethods();
  SharedPreferences prefs;
  StreamSubscription iosSubscription;
  static final NotificationHandler _singleton =
      new NotificationHandler._internal();
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  AndroidDeviceInfo androidInfo;
  IosDeviceInfo iosInfo;
  factory NotificationHandler() {
    return _singleton;
  }
  NotificationHandler._internal();

  initializeFcmNotification(BuildContext context) async {
    await initSharedPreference();
//    _listenForPermissionStatus();
//    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

//    var initializationSettingsAndroid =
//        new AndroidInitializationSettings('@mipmap/ic_launcher');
//    var initializationSettingsIOS = new IOSInitializationSettings(
//        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
//    var initializationSettings = new InitializationSettings(
//        initializationSettingsAndroid, initializationSettingsIOS);
//    flutterLocalNotificationsPlugin.initialize(initializationSettings,
//        onSelectNotification: onSelectNotification);

    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
        _saveDeviceToken();
      });

      _fcm.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
    } else {
      _saveDeviceToken();
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
//        final snackBar = SnackBar(
//            content: Text(
//                '${message['notification']['title']}! \n${message['notification']['body'] != null ? message['notification']['body'] : ''}'));
//        Constants.scaffoldKey.currentState.showSnackBar(snackBar);
      },
//      onBackgroundMessage: Theme.of(context).platform == TargetPlatform.iOS
//          ? null
//          : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        switch (message['data']['screen']) {
          case 'blog':
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(3));
            break;

          case 'message':
            BlocProvider.of<MessagesBloc>(context).add(LoadMessagesEvent());
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(4));

            break;
          case 'event':
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(1));

            break;
          case 'video':
            BlocProvider.of<VideoBloc>(context).add(LoadVideosEvent());
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => SermonsVideo()));

            break;
        }

//        await locator<NavigationService>()
//            .navigateTo(message['data']['screen']);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        switch (message['data']['screen']) {
          case 'blog':
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(3));
            break;

          case 'message':
            BlocProvider.of<MessagesBloc>(context).add(LoadMessagesEvent());
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(4));

            break;
          case 'event':
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(1));

            break;
          case 'video':
            BlocProvider.of<VideoBloc>(context).add(LoadVideosEvent());
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (_) => SermonsVideo()));

            break;
        }
//        await locator<NavigationService>()
//            .navigateTo(message['data']['screen']);
      },
    );
  }

//  void iOSPermission() {
//    _fcm.requestNotificationPermissions(
//        IosNotificationSettings(sound: true, badge: true, alert: true));
//    _fcm.onIosSettingsRegistered
//        .listen((IosNotificationSettings settings) {
//      print("Settings registered: $settings");
//    });
//  }

  Future getDeviceId() async {
    try {
      androidInfo = await deviceInfo.androidInfo;
    } catch (e) {
      print(e);
    }
  }

  Future getIosDeviceId() async {
    try {
      iosInfo = await deviceInfo.iosInfo;
    } catch (e) {
      print(e);
    }
  }

  /// Get the token, save it to the database for current user
  _saveDeviceToken() async {
    String fcmToken = await _fcm.getToken();
    print("FCM_TOKEN: $fcmToken");
    prefs = await SharedPreferences.getInstance();
    var deviceId;
    var platform;
    if (Platform.isIOS) {
//      iOSPermission();
      getIosDeviceId();
      platform = "IOS";
    } else {
      getDeviceId();
      platform = "ANDROID";
    }
    String clientSecret = prefs.get('clientSecret');
    if (clientSecret == null) {
      _fcm.getToken().then((token) {
        var deviceName;
        if (Platform.isIOS) {
          deviceId = iosInfo.identifierForVendor;
        } else {
          deviceId = androidInfo.androidId;
        }
//          var url = deviceName + "&fcmid=" + token;
        print('Token $token');

        Map<String, dynamic> body = {
          "device_id": deviceId,
          "fcm_id": token,
          "platform": platform
        };
        registerDevice(body);
//          _presenter.makeGetCall(Constants.REGISTRATION + url, PageName.MUSIC);
      });
    } else {
      print('ALERT!,Device is Already registred');
    }
  }

  initSharedPreference() async {
    prefs = await SharedPreferences.getInstance();
  }

//  Future<void> _showBigPictureNotification(message) async {
//    var rng = new Random();
//    var notifId = rng.nextInt(100);
//
//    var largeIconPath = await _downloadAndSaveImage(
//        'https://cdn.pixabay.com/photo/2019/04/21/21/29/pattern-4145023_960_720.jpg',
//        'largeIcon');
//    var bigPicturePath = await _downloadAndSaveImage(
//        'https://cdn.pixabay.com/photo/2019/04/21/21/29/pattern-4145023_960_720.jpg',
//        'bigPicture');
//    var bigPictureStyleInformation = BigPictureStyleInformation(
//        bigPicturePath, BitmapSource.FilePath,
//        largeIcon: largeIconPath,
//        largeIconBitmapSource: BitmapSource.FilePath,
//        contentTitle: message['data']['title'],
//        htmlFormatContentTitle: true,
//        summaryText: message['data']['body'],
//        htmlFormatSummaryText: true);
//    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//        '12', 'trading_id', message['data']['body'],
//        importance: Importance.High,
//        priority: Priority.High,
//        style: AndroidNotificationStyle.BigPicture,
//        styleInformation: bigPictureStyleInformation);
//    var platformChannelSpecifics =
//        NotificationDetails(androidPlatformChannelSpecifics, null);
//    await flutterLocalNotificationsPlugin.show(
//        notifId,
//        message['data']['title'],
//        message['data']['body'],
//        platformChannelSpecifics,
//        payload: message['data']['body']);
//  }
//
//  Future<void> _showBigTextNotification(message) async {
//    var rng = new Random();
//    var notifId = rng.nextInt(100);
//    var bigTextStyleInformation = BigTextStyleInformation(
//        message['data']['body'],
//        htmlFormatBigText: true,
//        contentTitle: message['data']['title'],
//        htmlFormatContentTitle: true,
//        summaryText: message['data']['body'],
//        htmlFormatSummaryText: true);
//    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//        '12', 'trading_id', '',
//        importance: Importance.Max,
//        priority: Priority.High,
//        style: AndroidNotificationStyle.BigText,
//        styleInformation: bigTextStyleInformation);
//    var platformChannelSpecifics =
//        NotificationDetails(androidPlatformChannelSpecifics, null);
//    await flutterLocalNotificationsPlugin.show(
//        notifId,
//        message['data']['title'],
//        message['data']['body'],
//        platformChannelSpecifics,
//        payload: message['data']['body']);
//  }
//
//  Future onSelectNotification(String payload) async {
//    if (payload != null) {
//      debugPrint('notification payload: ' + payload);
//    }
//    // await Navigator.push(
//    //   context,
//    //   new MaterialPageRoute(builder: (context) => new SecondScreen(payload)),
//    // );
//  }
//
//  Future<void> onDidReceiveLocalNotification(
//      int id, String title, String body, String payload) async {
//    // display a dialog with the notification details, tap ok to go to another page
//  }
//
//  Future<String> _downloadAndSaveImage(String url, String fileName) async {
//    var directory = await getApplicationDocumentsDirectory();
//    var filePath = '${directory.path}/$fileName';
//    var response = await http.get(url);
//    var file = File(filePath);
//    await file.writeAsBytes(response.bodyBytes);
//    return filePath;
//  }

  Future registerDevice(Map<String, dynamic> body) async {
    String fcmId = body['fcm_id'];
    String deviceId = body['device_id'];
    String platform = body['platform'];
    try {
      var response = await apiMethods
          .requestInGet(Constants.REGISTER_DEVICE_API + "/" + deviceId);
      Map<String, dynamic> rows =
          response != null ? jsonDecode(response) : null;
      if (rows != null) {
        Map<String, dynamic> responseJson = rows;
        if (responseJson.length == 0) {
          print('Registring new device');
          await apiMethods.requestInPost(
              Constants.REGISTER_DEVICE_API, null, body);
        } else {
          print('Device is already registered updating token..');
          await apiMethods.requestInPut(
              Constants.REGISTER_DEVICE_API + "/" + deviceId, null, body);
        }
      } else {
        print('Registring new device');
        await apiMethods.requestInPost(
            Constants.REGISTER_DEVICE_API, null, body);
      }

      bool operationStatus = await prefs.setString('clientSecret', fcmId);
      print('Operation status: $operationStatus');
      prefs.setString('deviceId', deviceId);
      prefs.setString('platform', platform);
    } catch (e) {
      print(e);
    }
  }
}
