class Constants {
  static const String phpApi =
      'https://rwc.christianappdevelopers.com/api/api.php/records/';
  static const String contact_us = phpApi + "contact_us";
  static const String mail =
      "https://rwc.christianappdevelopers.com/api/mail.php?";
  static const String prayer_request = phpApi + "prayer_requests";
  static const String about_us = phpApi + "about_us";
  static const String blogs = phpApi + "blogs?order=id,desc";
  static const String home_images = phpApi + "home_images";
  static const String audio = phpApi + "audio?order=id,desc";
  static const String YOUTUBE_APP_KEY =
      "AIzaSyDs1WCfQ1XpFFVICXJuAVw3UY-vjKcY090";

  static const String assetPath =
      "https://rwc.christianappdevelopers.com/public/storage/";

  static const String video =
      phpApi + "videos?order=date,desc&filter=substr_date,eq,";

  static const String VIDEO_DATES_API =
      phpApi + "videos?order=date,desc&include=substr_date";

  static const String upcoming_events = phpApi + "events?order=date,desc";

  static const String MESSAGE_API = phpApi + "messages?order=id,desc";

  static const String REGISTER_DEVICE_API = phpApi + "notification";
}
