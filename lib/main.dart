import 'dart:async';

import 'package:auto_orientation/auto_orientation.dart';
import 'package:device_info/device_info.dart';
import 'package:fluid_bottom_nav_bar/fluid_bottom_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:redfieldwesleyanchurch/api/api.dart';
import 'package:redfieldwesleyanchurch/blocs/home/home_bloc.dart';
import 'package:redfieldwesleyanchurch/blocs/messages/messages_bloc.dart';
import 'package:redfieldwesleyanchurch/blocs/video/video_bloc.dart';
import 'package:redfieldwesleyanchurch/shared/firebase_notification.dart';
import 'package:redfieldwesleyanchurch/widgets/about.dart';
import 'package:redfieldwesleyanchurch/widgets/blog.dart';
import 'package:redfieldwesleyanchurch/widgets/contact.dart';
import 'package:redfieldwesleyanchurch/widgets/home.dart';
import 'package:redfieldwesleyanchurch/widgets/messages.dart';
import 'package:redfieldwesleyanchurch/widgets/prayer_request.dart';
import 'package:redfieldwesleyanchurch/widgets/sermons.dart';
import 'package:redfieldwesleyanchurch/widgets/upcoming_events.dart';
import 'package:shared_preferences/shared_preferences.dart';

RmxAudioPlayer rmxAudioPlayer = new RmxAudioPlayer();
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
//  BlocSupervisor.delegate = SimpleBlocDelegate(
//    await HydratedBlocStorage.getInstance(),
//  );
  runApp(MultiBlocProvider(providers: [
    BlocProvider<VideoBloc>(
      create: (BuildContext context) => VideoBloc(),
    ),
    BlocProvider<MessagesBloc>(
      create: (BuildContext context) => MessagesBloc(),
    ),
    BlocProvider<HomeBloc>(
      create: (BuildContext context) => HomeBloc(),
    ),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Redfield Wesleyan Church',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        inputDecorationTheme: InputDecorationTheme(
            focusColor: Colors.red,
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF0f72a8)),
                borderRadius: BorderRadius.all(Radius.circular(15)))),
        accentColor: Color(0xFFcf5319),
        colorScheme: ColorScheme(
            primary: Color(0xFF1995CF),
            primaryVariant: Color(0xFF0f72a8),
            secondary: Color(0xFF6DBE45),
            secondaryVariant: Color(0xFF459531),
            surface: Color(0xFFFFFFFF),
            background: Color(0xFFFFFFFF),
            error: Color(0xFFB00020),
            onPrimary: Color(0xFFFFFFFF),
            onSecondary: Color(0xFF000000),
            onSurface: Color(0xFF000000),
            onBackground: Color(0xFF000000),
            onError: Color(0xFFFFFFFF),
            brightness: Brightness.light),
//        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Redfield Wesleyan Church'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Widget _child;
  String title;
  double _seeking;
  double _position = 0;

  int _current = 0;
  int _total = 0;

  String _status = 'none';
  ApiMethods apiMethods = ApiMethods();
  SharedPreferences prefs;
  StreamSubscription iosSubscription;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  AndroidDeviceInfo androidInfo;
  IosDeviceInfo iosInfo;

  @override
  void initState() {
    title = widget.title;
    _child = Home();
    super.initState();
    NotificationHandler().initializeFcmNotification(context);
//
    rmxAudioPlayer.initialize();
//
//    rmxAudioPlayer.on('status', (eventName, {dynamic args}) {
//      print(eventName + (args ?? "").toString());
//
//      if ((args as OnStatusCallbackData).value != null) {
//        setState(() {
//          if ((args as OnStatusCallbackData).value['currentPosition'] != null) {
//            _current =
//                (args as OnStatusCallbackData).value['currentPosition'].toInt();
//            _total = (((args as OnStatusCallbackData).value['duration']) ?? 0)
//                .toInt();
//            _status = (args as OnStatusCallbackData).value['status'];
//
//            if (_current > 0 && _total > 0) {
//              _position = _current / _total;
//            } else if (!rmxAudioPlayer.isLoading && !rmxAudioPlayer.isSeeking) {
//              _position = 0;
//            }
//
//            if (_seeking != null &&
//                !rmxAudioPlayer.isSeeking &&
//                !rmxAudioPlayer.isLoading) {
//              _seeking = null;
//            }
//          }
//        });
//      }
//    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(
              FontAwesomeIcons.prayingHands,
              color: Theme.of(context).colorScheme.primaryVariant,
            ),
            onPressed: () => Navigator.push(
                context, MaterialPageRoute(builder: (_) => PrayerRequest())),
          ),
          PopupMenuButton<String>(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.more_vert,
                  color: Theme.of(context).colorScheme.primaryVariant,
                ),
              ),
              onSelected: (selectedMenu) {
                if (selectedMenu == "contact-us") {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Contact()));
                } else if (selectedMenu == "about-us") {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => AboutUs()));
                }
              },
              itemBuilder: (context) => [
                    PopupMenuItem<String>(
                        value: "contact-us",
                        child: ListTile(
                          title: Text("Contact Us"),
                          leading: Icon(
                            Icons.phone,
                            color: Theme.of(context).colorScheme.primaryVariant,
                          ),
                        )),
                    PopupMenuItem<String>(
                        value: "about-us",
                        child: ListTile(
                          title: Text("About Us"),
                          leading: Icon(
                            Icons.info_outline,
                            color: Theme.of(context).colorScheme.primaryVariant,
                          ),
                        )),
                  ]),
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          title,
          style: TextStyle(color: Theme.of(context).colorScheme.primaryVariant),
        ),
      ),
      extendBody: true,
      body: BlocListener<HomeBloc, HomeState>(
        listener: (BuildContext context, HomeState state) {
          if (state is ChildChangedState) {
            _handleTitleChange(state.index);
          }
        },
        child: BlocBuilder<HomeBloc, HomeState>(
            builder: (context, HomeState state) {
          if (state is ChildChangedState) {
            return SafeArea(
                child: OfflineBuilder(
                    connectivityBuilder: (
                      BuildContext context,
                      ConnectivityResult connectivity,
                      Widget child,
                    ) {
                      final bool connected =
                          connectivity != ConnectivityResult.none;
                      return Stack(
                        fit: StackFit.expand,
                        children: [
                          child,
                          !connected
                              ? Positioned(
                                  height: 32.0,
                                  left: 0.0,
                                  right: 0.0,
                                  child: AnimatedContainer(
                                    duration: const Duration(milliseconds: 350),
                                    color: Color(0xFFEE4400),
                                    child: AnimatedSwitcher(
                                      duration:
                                          const Duration(milliseconds: 350),
                                      child: Text('Offline'),
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      );
                    },
                    child: _handleNavigationChange(state.index)));
          }
        }),
      ),
      bottomNavigationBar: FluidNavBar(
        icons: [
          FluidNavBarIcon(
              iconPath: "assets/house.svg", backgroundColor: Color(0xFF4285F4)),
          FluidNavBarIcon(
              iconPath: "assets/events.svg",
              backgroundColor: Color(0xFFEC4134)),
          FluidNavBarIcon(
              iconPath: "assets/play-button.svg",
              backgroundColor: Color(0xFFFCBA02)),
          FluidNavBarIcon(
              iconPath: "assets/blog.svg", backgroundColor: Color(0xFF34A950)),
          FluidNavBarIcon(
              iconPath: "assets/messages.svg",
              backgroundColor: Color(0xFF34A950)),
        ],
        onChange: (index) =>
            BlocProvider.of<HomeBloc>(context).add(ChangeChildEvent(index)),
        style: FluidNavBarStyle(
          iconUnselectedForegroundColor: Colors.white,
          barBackgroundColor: Theme.of(context).colorScheme.primaryVariant,
          iconBackgroundColor: Colors.green,
        ),
        scaleFactor: 1.5,
      ),
    );
  }

  Widget _handleNavigationChange(int index) {
    switch (index) {
      case 0:
        title = widget.title;

        _child = Home();
        break;
      case 1:
        title = "Upcoming Events";

        _child = UpcomingEvents();

        break;
      case 2:
        title = "Sermons";

        _child = Sermons(
          rmxAudioPlayer: rmxAudioPlayer,
        );

        break;
      case 3:
        title = "Blog";

        _child = Blog();
        break;
      case 4:
        BlocProvider.of<MessagesBloc>(context).add(LoadMessagesEvent());

        title = "Notifications";

        _child = Messages();
        break;
    }
    return _child = AnimatedSwitcher(
      switchInCurve: Curves.easeOut,
      switchOutCurve: Curves.easeIn,
      duration: Duration(milliseconds: 500),
      child: _child,
    );
  }

  void _handleTitleChange(int index) {
    switch (index) {
      case 0:
        setState(() {
          title = widget.title;
        });
        break;
      case 1:
        setState(() {
          title = "Upcoming Events";
        });

        break;
      case 2:
        setState(() {
          title = "Sermons";
        });

        break;
      case 3:
        setState(() {
          title = "Blog";
        });
        break;
      case 4:
        setState(() {
          title = "Notifications";
        });
        break;
    }
  }
}

//class SimpleBlocDelegate extends HydratedBlocDelegate {
//  SimpleBlocDelegate(HydratedStorage storage) : super(storage);
//
//  @override
//  void onEvent(Bloc bloc, Object event) {
//    super.onEvent(bloc, event);
//    print(event);
//  }
//
//  @override
//  onTransition(Bloc bloc, Transition transition) {
//    super.onTransition(bloc, transition);
//    print(transition);
//  }
//
//  @override
//  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
//    super.onError(bloc, error, stacktrace);
//    print(error);
//  }
//}
