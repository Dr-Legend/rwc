# redfieldwesleyanchurch

We are a family of people who believe in Jesus as the way, the truth, and the life. We desire to experience God’s love for us and to share God’s love with the people we encounter. Our worship is contemporary, casual, grounded in Scripture and Spirit empowered.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
